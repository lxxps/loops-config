<?php

/*
 * This file is part of the loops/config package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Loops\Config;

/**
 * Class to manage configuration.
 * 
 * To use it efficiently, you should create a class that extends it:
 * 
 * class MyConfig extends \Loops\Config\Base {}
 * 
 * Then, you can add any configuration and access it easly helping magic methods.
 * 
 * Set:
 * MyConfig::getInstance()->my_custom_setting = 'foo';
 * MyConfig::getInstance()->setMyCustomSetting( 'foo' );
 * MyConfig::setMyCustomSetting( 'foo' );
 * 
 * Get:
 * $c = MyConfig::getInstance()->my_custom_setting;
 * $c = MyConfig::getInstance()->getMyCustomSetting();
 * $c = MyConfig::getMyCustomSetting();
 * 
 * Is set:
 * isset( MyConfig::getInstance()->my_custom_setting );
 * MyConfig::getInstance()->hasMyCustomSetting();
 * MyConfig::hasMyCustomSetting();
 * 
 * Define - set if not exists:
 * MyConfig::getInstance()->defineMyCustomSetting( 'foo' );
 * MyConfig::defineMyCustomSetting( 'foo' );
 * 
 * Append - depends of setting type:
 * MyConfig::getInstance()->appendMyCustomSetting( 'foo' );
 * MyConfig::appendMyCustomSetting( 'foo' );
 * 
 * Prepend - depends of setting type:
 * MyConfig::getInstance()->prependMyCustomSetting( 'foo' );
 * MyConfig::prependMyCustomSetting( 'foo' );
 * 
 * This class follow POOP pattern, for further information see POOP file.
 *
 * @package    loops/config
 * @author     Loops <pierrotevrard@gmail.com>
 * @version    1.0
 */
class Base
{
  /**
   * An array of \Loops\Config\Base instance.
   *
   * @var array Array of \Loops\Config\Base
   * @access protected
   */
  public static $_instances = array();
  
  /**
   * Configuration array.
   *
   * @var array
   * @access protected
   */
  public $_config = array();
  
  /**
   * Method to retrieve \Loops\Config\Base instance.
   * 
   * If class name to retrieve is not specified, the method use 
   * get_called_class().
   *
   * @param  [string]  Class name
   * @return \Loops\Config\Base Expected instance
   * @access public
   * @static
   */ 
  public static function getInstance( $class = null )
  {
    $class or ( $class = get_called_class() );
    isset( static::$_instances[$class] ) or ( static::$_instances[$class] = new $class() );
    
    return static::$_instances[$class];
  }
  
  /**
   * Instance constructor.
   * Protected constructor to force use of getInstance() static method.
   * 
   * Keep in mind POOP pattern: the constructor should be protected, 
   * but it is not.
   *
   * @param none
   * @return void
   * @access protected
   */ 
  public function __construct()
  {
  }
  
  /**
   * Method to camelize a word for configuration setting name.
   * foo-bar => FooBar
   *
   * @param string $str String
   * @return string $str Camelized string
   * @access public
   * @static
   */ 
  public static function camelize( $str )
  {
    // apply camelize
    $str = preg_replace_callback( '~(^|[^0-9a-z]+)([0-9a-z])([0-9a-z]*)~i' , function( array $matches ){
      return strtoupper($matches[2]) . strtolower($matches[3]);
    } , $str );
    
    // remove unexpected characters
    return preg_replace( '~[^0-9a-z]+~i' , '' , $str );
  }
  
  /**
   * Method to dash a word for configuration setting name.
   * FooBar => foo-bar
   *
   * @param string $str String
   * @return string $str Dashed string
   * @access public
   * @static
   */ 
  public static function dash( $str )
  {
    // apply dash
    $str = preg_replace_callback( '~[0-9]+|([A-Z][a-z]*)~' , function( array $matches ) {
      return '-' . strtolower($matches[0]);
    } , $str );
    
    // remove unexpected characters
    return trim( preg_replace( '~[^0-9a-z]+~' , '-' , $str ) , '-' );
  }

  /**
   * Method to underscore a word for configuration setting name.
   * FooBar => foo_bar
   *
   * @param string $str String
   * @return string $str Dashed string
   * @access public
   * @static
   */
  public static function underscore( $str )
  {
    // apply underscore
    $str = preg_replace_callback( '~[0-9]+|([A-Z][a-z]*)~' , function( array $matches ) {
      return '_' . strtolower($matches[0]);
    } , $str );
    
    // remove unexpected characters
    return trim( preg_replace( '~[^0-9a-z]+~' , '_' , $str ) , '_' );
  }
  
  /**
   * Magic method to emulate static access to configuration setting.
   *
   * @param string $method Method name
   * @param array $args Arguments array
   * @access public
   * @static
   */ 
  public static function __callStatic( $method , $args )
  {
    return call_user_func_array( array( static::getInstance() , $method ) , $args );
  }
  
  /**
   * Magic method to retrieve configuration setting.
   *
   * @param string $setting Setting name
   * @return mixed Setting value
   * @access public
   */
  public function __get( $setting )
  {
    // note that we use array_key_exists instead of 
    // isset to allow null configuration
    if( array_key_exists( $setting , $this->_config ) )
    {
      return $this->_config[$setting];
    }
    
    // restore Notice
    trigger_error( sprintf( 'Undefined property: %s::$%s' , get_class( $this ) , $setting ) , E_USER_NOTICE );
    
  }
  
  /**
   * Magic method to assign configuration setting.
   *
   * @param string $setting Setting name
   * @param mixed  $value Setting value
   * @return void
   * @access public
   */
  public function __set( $setting , $value )
  {
    $this->_config[$setting] = $value;
  }
  
  /**
   * Magic method to detect configuration setting.
   *
   * @param string $setting Setting name
   * @return boolean
   * @access public
   */
  public function __isset( $setting )
  {
    // note that we use array_key_exists instead of 
    // isset to allow null configuration
    return array_key_exists( $setting , $this->_config );
  }
  
  /**
   * Magic method to unset configuration setting.
   *
   * @param string $setting Setting name
   * @return boolean
   * @access public
   */
  public function __unset( $setting )
  {
    unset( $this->_config[ $setting ] );
  }
  
  /**
   * Method to assign a value to a configuration setting only if it does not 
   * exist. Not that null value are not overwrote.
   *
   * @param string $setting Setting name
   * @param mixed  $value Setting value
   * @return void
   * @access public
   */
  public function define( $setting , $value )
  {
    isset( $this->$setting ) or ( $this->$setting = $value );
  }
  
  /**
   * Method to append a configuration setting with additionnal value.
   * 
   * Depends of the configuration setting type:
   * - array, use array union operator;
   * - string, use concatenation, separated by PATH_SEPARATOR for convenience;
   * - integer, use | operator;
   * - other, overwrote.
   *
   * @param  string $setting Setting name
   * @param  mixed  $value Setting value
   * @param  [string] $sep Path separator (string case)
   * @return void
   * @access public
   */
  public function append( $setting , $value , $sep = PATH_SEPARATOR )
  {
    // check existence
    if( isset($this->$setting) )
    {
      switch( true )
      {
        // array case
        case is_array($this->$setting) :
          $this->$setting += (array)$value;
        break;
      
        // string case
        case is_string($this->$setting) /** / && $this->$setting !== '' /* allow empty string? */ :
          $this->$setting .= $sep . (string)$value;
        break;
      
        // integer case
        case is_int($this->$setting) :
          $this->$setting |= (int)$value;
        break;
      
        default:
          $this->$setting = $value;
      }
    }
    else
    {
      $this->$setting = $value;
    }
  }
  
  /**
   * Method to prepend a configuration setting with additionnal value.
   * 
   * Depends of the configuration setting type:
   * - array, use array union operator;
   * - string, use concatenation, separated by PATH_SEPARATOR for convenience;
   * - integer, use | operator;
   * - other, overwrote.
   *
   * @param  string $setting Setting name
   * @param  mixed  $value Setting value
   * @param  [string] $sep Path separator (string case)
   * @return void
   * @access public
   */
  public function prepend( $setting , $value , $sep = PATH_SEPARATOR )
  {
    // check existence
    if( isset($this->$setting) )
    {
      switch( true )
      {
        // array case
        case is_array($this->$setting) :
          $this->$setting = (array)$value + $this->$setting;
        break;
      
        // string case
        case is_string($this->$setting) /** / && $this->$setting !== '' /* allow empty string? */ :
          $this->$setting = (string)$value . $sep . $this->$setting;
        break;
      
        // integer case
        case is_int($this->$setting) :
          $this->$setting |= (int)$value; // same than append
        break;
      
        default:
          $this->$setting = $value;
      }
    }
    else
    {
      $this->$setting = $value;
    }
  }
  
  
  /**
   * Magic method to emulate public methods to assign and
   * retrieve configuration setting.
   *
   * @param string $method Method name
   * @param array  $args Arguments array
   * @return mixed
   * @access public
   */
  public function __call( $method , $args )
  {
    switch( true )
    {
      // magic call to __set()
      case strpos( $method , 'set' ) === 0 :
        $this->{static::underscore( substr( $method , 3 ) )} = $args[0];
      break;
    
      // magic call to __get()
      case strpos( $method , 'get' ) === 0 :
        return $this->{static::underscore( substr( $method , 3 ) )};
      break;
    
      // magic call to __isset()
      case strpos( $method , 'has' ) === 0 :
        return isset( $this->{static::underscore( substr( $method , 3 ) )} );
      break;
    
      // magic call to __unset()
      case strpos( $method , 'unset' ) === 0 :
        unset( $this->{static::underscore( substr( $method , 5 ) )} );
      break;
    
      // call define()
      case strpos( $method , 'define' ) === 0 :
        $this->define( static::underscore( substr( $method , 6 ) ) , $args[0] );
      break;
    
      // call append()
      case strpos( $method , 'append' ) === 0 :
        array_key_exists( 1 , $args ) or ( $args[1] = PATH_SEPARATOR ); // make sure second argument is set
        $this->append( static::underscore( substr( $method , 6 ) ) , $args[0] , $args[1] );
      break;
    
      // call prepend()
      case strpos( $method , 'prepend' ) === 0 :
        array_key_exists( 1 , $args ) or ( $args[1] = PATH_SEPARATOR ); // make sure second argument is set
        $this->prepend( static::underscore( substr( $method , 7 ) ) , $args[0] , $args[1] );
      break;
    
      default:
        // restore Fatal Error
        trigger_error( sprintf( 'Call to undefined method %s::%s()' , get_class( $this ) , $method ) , E_USER_ERROR );
    }
  }
  
  /**
   * Method to dump all settings.
   *
   * @param none
   * @return array Array of settings
   * @access public
   */
  public function dump()
  {
    return $this->_config;
  }
  
  /**
   * Method to add an array of settings to the configuration.
   * This method does not reset the configuration, just to be safe.
   *
   * @param array $settings
   * @return none
   * @access public
   */
  public function load( $settings )
  {
    foreach( $settings as $key => $value )
    {
      // set it
      $this->$key = $value;
    }
  }
  
}
