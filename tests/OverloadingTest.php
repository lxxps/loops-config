<?php

/*
 * This file is part of the loops/config package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Loops\Config;

use Loops\Config\Base as Config;

/**
 * Overloading methods test suite for Loops\Config\Base
 *
 * @package    loops/config
 * @author     Loops <pierrotevrard@gmail.com>
 */
class OverloadingTest extends \PHPUnit_Framework_TestCase
{
  /*
   * Test case 1: Getters/setters
   */
  public function test1()
  {
    $config = new Config();
    
    // set via __set()
    $config->my_setting_1 = '1a';
    $this->assertSame( $config->my_setting_1 , '1a' );
    $this->assertSame( $config->getMySetting1() , '1a' );
    
    // set via __call()
    $config->setMySetting1( '1b' );
    $this->assertSame( $config->my_setting_1 , '1b' );
    $this->assertSame( $config->getMySetting1() , '1b' );
    
    // set via __set()
    $config->my_2setting = '2a';
    $this->assertSame( $config->my_2setting , '2a' );
    $this->assertSame( $config->getMy2setting() , '2a' );
    
    // set via __set()
    $config->setMy2setting( '2b' );
    $this->assertSame( $config->my_2setting , '2b' );
    $this->assertSame( $config->getMy2setting() , '2b' );
    
    // set via __set()
    $config->my_3_setting = '3a';
    $this->assertSame( $config->my_3_setting , '3a' );
    $this->assertSame( $config->getMy3Setting() , '3a' );
    
    // set via __set()
    $config->setMy3Setting( '3b' );
    $this->assertSame( $config->my_3_setting , '3b' );
    $this->assertSame( $config->getMy3Setting() , '3b' );
  }
  
  /*
   * Test case 2: isset
   */
  public function test2()
  {
    $config = new Config();
    
    // simple test
    $this->assertFalse( isset( $config->my_setting_1 ) );
    $this->assertFalse( $config->hasMySetting1() );
    
    // set via __set()
    $config->my_setting_2 = true;
    $this->assertTrue( isset( $config->my_setting_2 ) );
    $this->assertTrue( $config->hasMySetting2() );
    
    // set via __call()
    $config->setMySetting3( true );
    $this->assertTrue( isset( $config->my_setting_3 ) );
    $this->assertTrue( $config->hasMySetting3() );
    
    // simple test
    $this->assertFalse( isset( $config->my_setting_4 ) );
    $this->assertFalse( $config->hasMySetting4() );
  }
  
  /*
   * Test case 3: unset
   */
  public function test3()
  {
    $config = new Config();
    
    // unset via __unset()
    $config->setMySetting1( true );
    unset( $config->my_setting_1 );
    $this->assertFalse( $config->hasMySetting1() );
    
    // unset via __call()
    $config->setMySetting1( true );
    $config->unsetMySetting1();
    $this->assertFalse( $config->hasMySetting1() );
  }
  
  /*
   * Test case 4: isset "false" value
   */
  public function test4()
  {
    $config = new Config();
    
    // set a false value
    $config->setMySetting1( false );
    $this->assertTrue( isset( $config->my_setting_1 ) );
    
    // set a null value
    $config->setMySetting2( null );
    $this->assertTrue( isset( $config->my_setting_2 ) );
    
    // set a empty string value
    $config->setMySetting3( '' );
    $this->assertTrue( isset( $config->my_setting_3 ) );
    
    // set a empty array value
    $config->setMySetting4( array() );
    $this->assertTrue( isset( $config->my_setting_4 ) );
    
    // set a 0 value
    $config->setMySetting5( 0 );
    $this->assertTrue( isset( $config->my_setting_5 ) );
  }
  
  /*
   * Test case 5: isset value (interoperability failure)
   */
  public function test5()
  {
    $config = new Config();
    
    // this setting cannot be magically accessed
    $config->mysetting1 = true;
    $this->assertTrue( isset( $config->mysetting1 ) );
    // error control on "Undefined property: $mysetting_1"
    $this->assertNull( @ $config->getMysetting1() ); 
    
    // this setting cannot be magically accessed
    $config->my2_setting = true;
    $this->assertTrue( isset( $config->my2_setting ) );
    // error control on "Undefined property: $my_2_setting"
    $this->assertNull( @ $config->getMy2Setting() );
  }
  
}
