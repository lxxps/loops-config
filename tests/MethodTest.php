<?php

/*
 * This file is part of the loops/config package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Loops\Config;

use Loops\Config\Base as Config;

/**
 * Methods test suite for Loops\Config\Base
 *
 * @package    loops/config
 * @author     Loops <pierrotevrard@gmail.com>
 */
class MethodTest extends \PHPUnit_Framework_TestCase
{
  /*
   * Test case 1: define
   */
  public function test1()
  {
    $config = new Config();
    
   
    // define via define
    $config->define( 'my_setting_1' , '1a' );
    $this->assertSame( $config->getMySetting1() , '1a' );
   
    // define via __call
    $config->defineMySetting2( '2a' );
    $this->assertSame( $config->getMySetting2() , '2a' );
    
   
    // define/set/define a value
    $config->defineMySetting3( '3a' );
    $this->assertSame( $config->getMySetting3() , '3a' );
    
    $config->setMySetting3( '3b' );
    $this->assertSame( $config->getMySetting3() , '3b' );
    
    $config->defineMySetting3( '3c' );
    $this->assertNotSame( $config->getMySetting3() , '3c' );
  }
  
  /*
   * Test case 2: append/prepend array
   */
  public function test2()
  {
    $config = new Config();
    
    
    // array to array
    
    // append via append
    $config->setMySetting1( array( 'a' => '1a' , 'b' => '1b' , ) );
    $config->append( 'my_setting_1' , array( 'a' => '2a' , 'c' => '2c' , ) );
    $this->assertSame( $config->getMySetting1() , array( 'a' => '1a' , 'b' => '1b' , 'c' => '2c' , ) );
   
    // append via __call
    $config->setMySetting2( array( 'a' => '1a' , 'b' => '1b' , ) );
    $config->appendMySetting2( array( 'a' => '2a' , 'c' => '2c' , ) );
    $this->assertSame( $config->getMySetting2() , array( 'a' => '1a' , 'b' => '1b' , 'c' => '2c' , ) );
    
    // prepend via prepend
    $config->setMySetting3( array( 'a' => '1a' , 'b' => '1b' , ) );
    $config->prepend( 'my_setting_3' , array( 'a' => '2a' , 'c' => '2c' , ) );
    $this->assertSame( $config->getMySetting3() , array( 'a' => '2a' , 'c' => '2c' , 'b' => '1b' , ) );
   
    // prepend via __call
    $config->setMySetting4( array( 'a' => '1a' , 'b' => '1b' , ) );
    $config->prependMySetting4( array( 'a' => '2a' , 'c' => '2c' , ) );
    $this->assertSame( $config->getMySetting4() , array( 'a' => '2a' , 'c' => '2c' , 'b' => '1b' , ) );
    
    
    // not array to array with string keys
    
    // append
    $config->setMySetting5( array( 'a' => '1a' , 'b' => '1b' , ) );
    $config->appendMySetting5( '2c' );
    $this->assertSame( $config->getMySetting5() , array( 'a' => '1a' , 'b' => '1b' , '2c' , ) );
    
    // prepend
    $config->setMySetting6( array( 'a' => '1a' , 'b' => '1b' , ) );
    $config->prependMySetting6( '2c' );
    $this->assertSame( $config->getMySetting6() , array( '2c' , 'a' => '1a' , 'b' => '1b' , ) );
    
    
    // not array to array with integer keys
    
    // append
    $config->setMySetting7( array( '1a' , '1b' , ) );
    $config->appendMySetting7( '2c' );
    // yes, the value won't be added because key 0 is already set
    $this->assertSame( $config->getMySetting7() , array( '1a' , '1b' , ) );
    
    // prepend
    $config->setMySetting8( array( '1a' , '1b' , ) );
    $config->prependMySetting8( '2c' );
    $this->assertSame( $config->getMySetting8() , array( '2c' , '1b' , ) );
    
    
    // null to array
    
    // append
    $config->setMySetting9( array( '1a' , '1b' , ) );
    $config->appendMySetting9( null );
    $this->assertSame( $config->getMySetting9() , array( '1a' , '1b' , ) );
    
    // prepend
    $config->setMySetting10( array( '1a' , '1b' , ) );
    $config->prependMySetting10( null );
    $this->assertSame( $config->getMySetting10() , array( '1a' , '1b' , ) );
    
  }
  
  /*
   * Test case 3: append/prepend string
   */
  public function test3()
  {
    $config = new Config();
    
    
    // string to string
    
    // append via append
    $config->setMySetting1( '1a' );
    $config->append( 'my_setting_1' , '2b' );
    $this->assertSame( $config->getMySetting1() , '1a'.PATH_SEPARATOR.'2b' );
   
    // append via __call
    $config->setMySetting2( '1a' );
    $config->appendMySetting2( '2b' );
    $this->assertSame( $config->getMySetting2() , '1a'.PATH_SEPARATOR.'2b' );
    
    // prepend via prepend
    $config->setMySetting3( '1a' );
    $config->prepend( 'my_setting_3' , '2b' );
    $this->assertSame( $config->getMySetting3() , '2b'.PATH_SEPARATOR.'1a' );
   
    // prepend via __call
    $config->setMySetting4( '1a' );
    $config->prependMySetting4( '2b' );
    $this->assertSame( $config->getMySetting4() , '2b'.PATH_SEPARATOR.'1a' );
   
    
    // string to string with custom separator
   
    // append
    $config->setMySetting5( '1a' );
    $config->appendMySetting5( '2b' , '|' );
    $this->assertSame( $config->getMySetting5() , '1a|2b' );
   
    // prepend
    $config->setMySetting6( '1a' );
    $config->prependMySetting6( '2b' , '|' );
    $this->assertSame( $config->getMySetting6() , '2b|1a' );
    
    
    // integer to string
   
    // append
    $config->setMySetting7( '1a' );
    $config->appendMySetting7( 2 );
    $this->assertSame( $config->getMySetting7() , '1a'.PATH_SEPARATOR.'2' );
   
    // prepend
    $config->setMySetting8( '1a' );
    $config->prependMySetting8( 2 );
    $this->assertSame( $config->getMySetting8() , '2'.PATH_SEPARATOR.'1a' );
    
    
    // array to string
   
    // append
    $config->setMySetting9( '1a' );
    // error control on "Array to string conversion"
    @ $config->appendMySetting9( array( '2b' ) );
    $this->assertSame( $config->getMySetting9() , '1a'.PATH_SEPARATOR.'Array' );
   
    // prepend
    $config->setMySetting10( '1a' );
    // error control on "Array to string conversion"
    @ $config->prependMySetting10( array( '2b' ) );
    $this->assertSame( $config->getMySetting10() , 'Array'.PATH_SEPARATOR.'1a' );
    
    
    // null to string
   
    // append
    $config->setMySetting11( '1a' );
    $config->appendMySetting11( null );
    $this->assertSame( $config->getMySetting11() , '1a'.PATH_SEPARATOR );
    // disallow empty string? $this->assertSame( $config->getMySetting11() , '1a' );
   
    // prepend
    $config->setMySetting12( '1a' );
    $config->prependMySetting12( null );
    $this->assertSame( $config->getMySetting12() , PATH_SEPARATOR.'1a' );
    // disallow empty string? $this->assertSame( $config->getMySetting11() , '1a' );
    
  }
  
  /*
   * Test case 4: append/prepend integer
   */
  public function test4()
  {
    $config = new Config();
    
    
    // string to string
   
    // append via append
    $config->setMySetting1( 1 );
    $config->append( 'my_setting_1' , 2 );
    $this->assertSame( $config->getMySetting1() , 3 );
   
    // append via __call
    $config->setMySetting2( 1 );
    $config->appendMySetting2( 2 );
    $this->assertSame( $config->getMySetting2() , 3 );
   
    // prepend via prepend
    $config->setMySetting3( 1 );
    $config->prepend( 'my_setting_3' , 2 );
    $this->assertSame( $config->getMySetting3() , 3 );
   
    // prepend via __call
    $config->setMySetting4( 1 );
    $config->prependMySetting4( 2 );
    $this->assertSame( $config->getMySetting4() , 3 );
   
    
    // string to integer
   
    // append
    $config->setMySetting5( 1 );
    $config->appendMySetting5( 'b' );
    $this->assertSame( $config->getMySetting5() , 1 );
   
    // prepend
    $config->setMySetting6( 1 );
    $config->prependMySetting6( 'b' );
    $this->assertSame( $config->getMySetting6() , 1 );
   
    
    // array to integer
   
    // append
    $config->setMySetting7( 1 );
    $config->appendMySetting7( array( '2b' ) );
    $this->assertSame( $config->getMySetting7() , 1 );
   
    // prepend
    $config->setMySetting8( 1 );
    $config->prependMySetting8( array( '2b' ) );
    $this->assertSame( $config->getMySetting8() , 1 );
   
    
    // numeric string to integer
   
    // append
    $config->setMySetting9( 1 );
    $config->appendMySetting9( '2' );
    $this->assertSame( $config->getMySetting9() , 3 );
   
    // prepend
    $config->setMySetting10( 1 );
    $config->prependMySetting10( '2' );
    $this->assertSame( $config->getMySetting10() , 3 );
   
    
    // null to integer
   
    // append
    $config->setMySetting9( 1 );
    $config->appendMySetting9( null );
    $this->assertSame( $config->getMySetting9() , 1 );
   
    // prepend
    $config->setMySetting10( 1 );
    $config->prependMySetting10( null );
    $this->assertSame( $config->getMySetting10() , 1 );
    
  }
  
  /*
   * Test case 5: append/prepend other types
   */
  public function test5()
  {
    $config = new Config();
    
    
    // float case
    
    // append
    $config->setMySetting1( 3.1416 );
    $config->appendMySetting1( 6.2832 );
    $this->assertSame( $config->getMySetting1() , 6.2832 );
    
    // prepend
    $config->setMySetting2( 3.1416 );
    $config->prependMySetting2( 6.2832 );
    $this->assertSame( $config->getMySetting2() , 6.2832 );
    
    
    // null case
    
    // append
    $config->setMySetting3( null );
    $config->appendMySetting3( true );
    $this->assertSame( $config->getMySetting3() , true );
    
    // prepend
    $config->setMySetting4( null );
    $config->prependMySetting4( true );
    $this->assertSame( $config->getMySetting4() , true );
    
    
    // true case
    
    // append
    $config->setMySetting5( true );
    $config->appendMySetting5( false );
    $this->assertSame( $config->getMySetting5() , false );
    
    // prepend
    $config->setMySetting6( true );
    $config->prependMySetting6( false );
    $this->assertSame( $config->getMySetting6() , false );
    
    
    // false case
    
    // append
    $config->setMySetting7( false );
    $config->appendMySetting7( true );
    $this->assertSame( $config->getMySetting7() , true );
    
    // prepend
    $config->setMySetting8( false );
    $config->prependMySetting8( true );
    $this->assertSame( $config->getMySetting8() , true );
  }
  
  /*
   * Test case 6: append/prepend when setting does not exists
   */
  public function test6()
  {
    $config = new Config();
    
    
    // append
    $config->appendMySetting1( 1 );
    $this->assertSame( $config->getMySetting1() , 1 );
    
    // prepend
    $config->prependMySetting2( 1 );
    $this->assertSame( $config->getMySetting2() , 1 );
  }
  
  /*
   * Test case 7: dump
   */
  public function test7()
  {
    $config = new Config();
    
    // dump
    $config->setMySetting1( '1a' );
    $config->setMySetting2( '2a' );
    $config->setMySetting3( '3a' );
    $config->setMySetting4( '4a' );
    $this->assertSame( $config->dump() , array( 
      'my_setting_1' => '1a' ,
      'my_setting_2' => '2a' ,
      'my_setting_3' => '3a' ,
      'my_setting_4' => '4a' ,
    ) );
  }
  
  /*
   * Test case 8: load
   */
  public function test8()
  {
    $config = new Config();
    
    // load
    $config->load( array( 
      'my_setting_1' => '1a' ,
      'my_setting_2' => '2a' ,
      'my_setting_3' => '3a' ,
      'my_setting_4' => '4a' ,
    ) );
    $this->assertSame( $config->getMySetting1() , '1a' );
    $this->assertSame( $config->getMySetting2() , '2a' );
    $this->assertSame( $config->getMySetting3() , '3a' );
    $this->assertSame( $config->getMySetting4() , '4a' );
    
    
    // no reset on load
    
    // load
    $config->load( array( 
      'my_setting_5' => '5a' ,
      'my_setting_6' => '6a' ,
    ) );
    $this->assertSame( $config->getMySetting1() , '1a' );
    $this->assertSame( $config->getMySetting2() , '2a' );
    $this->assertSame( $config->getMySetting3() , '3a' );
    $this->assertSame( $config->getMySetting4() , '4a' );
    $this->assertSame( $config->getMySetting5() , '5a' );
    $this->assertSame( $config->getMySetting6() , '6a' );
    
    
    // change values on load
    
    // load
    $config->load( array( 
      'my_setting_3' => '3b' ,
      'my_setting_4' => '4b' ,
      'my_setting_5' => '5b' ,
      'my_setting_6' => '6b' ,
    ) );
    $this->assertSame( $config->getMySetting1() , '1a' );
    $this->assertSame( $config->getMySetting2() , '2a' );
    $this->assertSame( $config->getMySetting3() , '3b' );
    $this->assertSame( $config->getMySetting4() , '4b' );
    $this->assertSame( $config->getMySetting5() , '5b' );
    $this->assertSame( $config->getMySetting6() , '6b' );
  }
  
}
