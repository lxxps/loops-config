<?php

/*
 * This file is part of the loops/config package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Loops\Config;

use Loops\Config\Base as Config;

/**
 * String manipulations test suite for Loops\Config\Base
 *
 * @package    loops/config
 * @author     Loops <pierrotevrard@gmail.com>
 */
class StringTransformTest extends \PHPUnit_Framework_TestCase
{
  /*
   * Test case 1: camelize
   */
  public function test1()
  {
    $this->assertSame( Config::camelize( 'my_setting_1' ) , 'MySetting1' );
    $this->assertSame( Config::camelize( 'my_setting1' ) , 'MySetting1' ); // note that this one return same value than above
    $this->assertSame( Config::camelize( 'my_2_setting' ) , 'My2Setting' );
    $this->assertSame( Config::camelize( 'my_2setting' ) , 'My2setting' );
  }
  
  /*
   * Test case 1: underscore
   */
  public function test2()
  {
    $this->assertSame( Config::underscore( 'MySetting1' ) , 'my_setting_1' );
    $this->assertSame( Config::underscore( 'Mysetting1' ) , 'mysetting_1' );
    
    $this->assertSame( Config::underscore( 'My2Setting' ) , 'my_2_setting' );
    $this->assertSame( Config::underscore( 'My2setting' ) , 'my_2setting' );
    
    $this->assertSame( Config::underscore( 'My30Setting' ) , 'my_30_setting' );
    $this->assertSame( Config::underscore( 'My30setting' ) , 'my_30setting' );
  }
  
  /*
   * Test case 3: dash
   */
  public function test3()
  {
    $this->assertSame( Config::dash( 'MySetting1' ) , 'my-setting-1' );
    $this->assertSame( Config::dash( 'Mysetting1' ) , 'mysetting-1' );
    
    $this->assertSame( Config::dash( 'My2Setting' ) , 'my-2-setting' );
    $this->assertSame( Config::dash( 'My2setting' ) , 'my-2setting' );
    
    $this->assertSame( Config::dash( 'My30Setting' ) , 'my-30-setting' );
    $this->assertSame( Config::dash( 'My30setting' ) , 'my-30setting' );
  }
  
}
