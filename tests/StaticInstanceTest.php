<?php

/*
 * This file is part of the loops/config package.
 * (c) Loops <pierrotevrard@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Loops\Config;

/**
 * Static behaviors suite for Loops\Config\Base
 *
 * @package    loops/config
 * @author     Loops <pierrotevrard@gmail.com>
 */
class StaticInstanceTest extends \PHPUnit_Framework_TestCase
{
  
  /*
   * Test case 1: Magic __callStatic method
   */
  public function test1()
  {
    // create dummy configuration
    eval( 'namespace Loops\\Config; class MyConfig1 extends Base {}' );
    
    // set/get
    MyConfig1::setMySetting1( '1a' );
    $this->assertSame( MyConfig1::getMySetting1() , '1a' );
    
    // has
    $this->assertTrue( MyConfig1::hasMySetting1() );
    $this->assertFalse( MyConfig1::hasMySetting2() );
    
    // define
    MyConfig1::defineMySetting2( '2a' );
    $this->assertSame( MyConfig1::getMySetting2() , '2a' );
    
    // define existing value
    MyConfig1::defineMySetting2( '2b' );
    $this->assertSame( MyConfig1::getMySetting2() , '2a' );
    
    // append
    MyConfig1::setMySetting3( array( '3a' ) );
    MyConfig1::appendMySetting3( array( '3b' ) );
    $this->assertSame( MyConfig1::getMySetting3() , array( '3a' ) );
    
    // prepend
    MyConfig1::setMySetting4( array( '4a' ) );
    MyConfig1::prependMySetting4( array( '4b' ) );
    $this->assertSame( MyConfig1::getMySetting4() , array( '4b' ) );
  }
  
  /*
   * Test case 2: class name collision
   */
  public function test2()
  {
    // create dummy configuration
    eval( 'namespace Loops\\Config; class MyConfig2a extends Base {}' );
    eval( 'namespace Loops\\Config; class MyConfig2b extends Base {}' );
    
    
    MyConfig2a::setMySetting1( '1a' );
    $this->assertFalse( MyConfig2b::hasMySetting1() );
    
    MyConfig2b::setMySetting2( '2a' );
    $this->assertFalse( MyConfig2a::hasMySetting2() );
  }
  
  /*
   * Test case 3: namespace collision
   */
  public function test3()
  {
    // create dummy configuration
    eval( 'namespace Dummy\\PackageA; class MyConfig extends \\Loops\\Config\\Base {}' );
    eval( 'namespace Dummy\\PackageB; class MyConfig extends \\Loops\\Config\\Base {}' );
    
    \Dummy\PackageA\MyConfig::setMySetting1( '1a' );
    $this->assertFalse( \Dummy\PackageB\MyConfig::hasMySetting1() );
    
    \Dummy\PackageB\MyConfig::setMySetting2( '2a' );
    $this->assertFalse( \Dummy\PackageA\MyConfig::hasMySetting2() );
  }
  
  /*
   * Test case 4: class alias
   */
  public function test4()
  {
    // create dummy configuration
    eval( 'namespace Loops\Config; class MyConfig4 extends Base {}' );
    // do an alias
    class_alias( '\\Loops\\Config\\MyConfig4' , '\\Test4_Config' );
    
    
    MyConfig4::setMySetting1( '1a' );
    $this->assertSame( \Test4_Config::getMySetting1() , '1a' );
    
    \Test4_Config::setMySetting2( '2a' );
    $this->assertSame( MyConfig4::getMySetting2() , '2a' );
  }
  
}
