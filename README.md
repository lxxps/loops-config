**Loops\Config**
================

Base class for a simple (ie. basic) configuration settings management.



Requirements:
-------------

- At least PHP 5.3.
- Loops\Autoloader package.



How to use it:
--------------


### Autoloading

If you do not have composer, call bootstrap:

    :::php
      require $path_to_package.'/bootstrap.inc.php';

*This library use package-oriented autoloading with a directory for package 
namespace (PSR-4) and underscore as directory separator for class names (PSR-0).*


### Extend

The `\Loops\Config\Base` class has been designed to be extended in order to 
isolate a package/bundle/project/component configuration.

    :::php
      class MyConfig extends \Loops\Config\Base {}


### Magic getter/setter/isset

Then, you should be able to assign/retrieve any setting:

    :::php
      // set "my_custom_setting"
      MyConfig::getInstance()->my_custom_setting = 'foo';
      MyConfig::getInstance()->setMyCustomSetting( 'foo' );
      MyConfig::setMyCustomSetting( 'foo' );
 
      // get "my_custom_setting"
      $s = MyConfig::getInstance()->my_custom_setting;
      $s = MyConfig::getInstance()->getMyCustomSetting();
      $s = MyConfig::getMyCustomSetting();

      // "my_custom_setting" is set?
      isset( MyConfig::getInstance()->my_custom_setting );
      MyConfig::getInstance()->hasMyCustomSetting();
      MyConfig::hasMyCustomSetting();

      // set "my_custom_setting", if not exists
      MyConfig::getInstance()->defineMyCustomSetting( 'foo' );
      MyConfig::defineMyCustomSetting( 'foo' );


### Prepend/append

Depending of your setting type, you could prepend/append some values:

    :::php
      //// array case

      // prepend
      MyConfig::setMyCustomSetting( array( 0 => 'foo0' , 1 => 'bar1' ) );
      MyConfig::prependMyCustomSetting( array( 0 => 'bar0' , 2 => 'foo2' ) );
      $s = MyConfig::getMyCustomSetting(); // result to array( 'bar0' , 'bar1' , 'foo2' )

      // append
      MyConfig::setMyCustomSetting( array( 0 => 'foo0' , 1 => 'bar1' ) );
      MyConfig::appendMyCustomSetting( array( 0 => 'bar0' , 2 => 'foo2' ) );
      $s = MyConfig::getMyCustomSetting(); // result to array( 'foo0' , 'bar1' , 'foo2' )


      //// string case

      // prepend
      MyConfig::setMyCustomSetting( 'foo' );
      MyConfig::prependMyCustomSetting( 'bar' );
      $s = MyConfig::getMyCustomSetting(); // result to 'bar'.PATH_SEPARATOR.'foo'

      // append
      MyConfig::setMyCustomSetting( 'foo' );
      MyConfig::appendMyCustomSetting( 'bar' );
      $s = MyConfig::getMyCustomSetting(); // result to 'foo'.PATH_SEPARATOR.'bar'

      // prepend with custom separator
      MyConfig::setMyCustomSetting( 'foo' );
      MyConfig::prependMyCustomSetting( 'bar' , '|' );
      $s = MyConfig::getMyCustomSetting(); // result to 'bar|foo'

      // append with custom separator
      MyConfig::setMyCustomSetting( 'foo' );
      MyConfig::appendMyCustomSetting( 'bar' , '|' );
      $s = MyConfig::getMyCustomSetting(); // result to 'foo|bar'


      //// integer case

      // prepend
      MyConfig::setMyCustomSetting( 4 );
      MyConfig::prependMyCustomSetting( 2 );
      $s = MyConfig::getMyCustomSetting(); // result to 6 (4|2)

      // append
      MyConfig::setMyCustomSetting( 4 );
      MyConfig::appendMyCustomSetting( 2 );
      $s = MyConfig::getMyCustomSetting(); // result to 6 (4|2)


### Load/dump

Useful method to dump/load current configuration.

    :::php
      // dump
      $s = MyConfig::->getInstance()->dump();

      // load
      MyConfig::->getInstance()->load( $s );



Limitations:
------------


### Number in property name

You may encounter issue with the camelize/underscore transformation, especially
in theses cases:

    :::php
      // 'my_setting1' => 'MySetting1' => 'my_setting_1'
      $s = MyConfig::underscore( MyConfig::camelize( 'my_setting1' ) ); // result to 'my_setting_1'

      // 'my2_setting' => 'My2Setting' => 'my_2_setting'
      $s = MyConfig::underscore( MyConfig::camelize( 'my2_setting' ) ); // result to 'my_2_setting'

There is no way to logically determine if it is better to keep underscores 
before number or not, so be aware of that behavior.


### append/prepend empty string

For now, the append/prepend method append/prepend empty string (the developer
may have a good reason to do it).

    :::php
      MyConfig::setMySetting1( 'a' );
      MyConfig::appendMySetting1( '' );
      $s = MyConfig::getMySetting1(); // result to 'a'.PATH_SEPARATOR

      MyConfig::setMySetting1( 'a' );
      MyConfig::prependMySetting1( '' );
      $s = MyConfig::getMySetting1(); // result to PATH_SEPARATOR.'a'



Contributors:
-------------

- Pierrot Evrard aka Loops — [https://twitter.com/lxxps](https://twitter.com/lxxps)


### Wanna contribute?

There is only one rule to follow: **Challenge yourself**.